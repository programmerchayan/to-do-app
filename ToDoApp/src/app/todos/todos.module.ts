import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { TodoComponent } from 'src/app/todos/Components/todo/todo.component';
import { HeaderComponent } from 'src/app/todos/Components/header/header.component';
import { TodosService } from 'src/app/todos/Services/todoService.service';
import { MainComponent } from 'src/app/todos/Components/main/main.component';
import { TodoLogicComponent } from 'src/app/todos/Components/todo-logic/todo-logic.component';
import { FooterComponent } from 'src/app/todos/Components/footer/footer.component';

const routes: Routes = [
  {
    path: '',
    component: TodoComponent,
  },
];

@NgModule({
  declarations: [
    TodoComponent,
    HeaderComponent,
    MainComponent,
    TodoLogicComponent,
    FooterComponent,
  ],
  imports: [CommonModule, RouterModule.forChild(routes)],
  providers: [TodosService],
})
export class TodoModule {}
