import { Component } from '@angular/core';
import { map, Observable } from 'rxjs';

import { TodosService } from 'src/app/todos/Services/todoService.service';
import { FilterEnum } from 'src/app/todos/DataTypes/filter.enum';

@Component({
  selector: 'app-todos-footer',
  templateUrl: './footer.component.html',
})
export class FooterComponent {
  noTodoClass$: Observable<boolean>;
  activeCount$: Observable<number>;
  itemLeftText$: Observable<string>;
  filter$: Observable<FilterEnum>;
  filterEnum = FilterEnum;

  constructor(private todoService: TodosService) {
    this.noTodoClass$ = this.todoService.todos$.pipe(
      map((todos) => todos.length === 0)
    );
    this.activeCount$ = this.todoService.todos$.pipe(
      map((todos) => todos.filter((todo) => !todo.isCompleted).length)
    );
    this.itemLeftText$ = this.activeCount$.pipe(
      map((activeCount) => `item${activeCount != 1 ? 's' : ''} Left`)
    );
    this.filter$ = this.todoService.filter$;
  }

  changeFilter(event: Event, filterName: FilterEnum): void {
    event.preventDefault();
    this.todoService.changeFilter(filterName);
  }
}
