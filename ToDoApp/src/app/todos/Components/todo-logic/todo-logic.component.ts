import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  ViewChild,
  ElementRef,
  OnChanges,
  SimpleChanges,
} from '@angular/core';

import { TodoInterface } from '../../DataTypes/todoInerface';
import { TodosService } from '../../Services/todoService.service';

@Component({
  selector: 'app-todo-logic',
  templateUrl: './todo-logic.component.html',
})
export class TodoLogicComponent implements OnInit, OnChanges {
  @Input('sendTodo') todoProp!: TodoInterface;
  @Input('isEditing') isEditingProp!: boolean;
  @Output('sendEditingId') sendEditingIdEvent: EventEmitter<string | null> =
    new EventEmitter();

  @ViewChild('textInput') textInput!: ElementRef;

  editingText: string = '';

  constructor(private todoService: TodosService) {}

  ngOnInit(): void {
    this.editingText = this.todoProp.text;
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('Changes ', changes);
    if (changes['isEditingProp'].currentValue) {
      setTimeout(() => {
        this.textInput.nativeElement.focus();
      }, 0);
    }
  }

  editTodoItem(): void {
    this.sendEditingIdEvent.emit(this.todoProp.id);
  }

  removeTodo(): void {
    this.todoService.removeTodo(this.todoProp.id);
  }

  toggleTodo(): void {
    this.todoService.toggleTodo(this.todoProp.id);
  }

  changeText(event: Event): void {
    const value = (event.target as HTMLInputElement).value;
    this.editingText = value;
  }

  changeTodo(): void {
    console.log('changeTodo ', this.editingText);
    this.todoService.changeTodoAfterEditing(this.todoProp.id, this.editingText);
    this.sendEditingIdEvent.emit(null);
    // set editing id to null closes the diting mode.
  }
}
