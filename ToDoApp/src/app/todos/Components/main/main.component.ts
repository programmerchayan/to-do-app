import { Component } from '@angular/core';
import { combineLatest, map, Observable } from 'rxjs';

import { TodoInterface } from 'src/app/todos/DataTypes/todoInerface';
import { TodosService } from 'src/app/todos/Services/todoService.service';
import { FilterEnum } from 'src/app/todos/DataTypes/filter.enum';

@Component({
  selector: 'app-todos-main',
  templateUrl: './main.component.html',
})
export class MainComponent {
  visibleTodos$: Observable<TodoInterface[]>;
  noTodosClass$: Observable<boolean>;
  isAllTodoSelected$: Observable<boolean>;
  editingId: string | null = null;

  constructor(private todoService: TodosService) {
    this.isAllTodoSelected$ = this.todoService.todos$.pipe(
      map((todos) => todos.every((todo) => todo.isCompleted))
    );
    this.noTodosClass$ = this.todoService.todos$.pipe(
      map((todos) => todos.length === 0)
    );
    this.visibleTodos$ = combineLatest(
      this.todoService.todos$,
      this.todoService.filter$
    ).pipe(
      map(([todos, filter]: [TodoInterface[], FilterEnum]) => {
        if (filter === FilterEnum.active) {
          return todos.filter((todo) => !todo.isCompleted);
        } else if (filter === FilterEnum.completed) {
          return todos.filter((todo) => todo.isCompleted);
        }
        return todos;
      })
    );
    // CombineLatest() combines several streams.
    // We use pipe(0 to call list of functions on stream.)
    // every() returns true if every element fits the condition
  }

  public toggleAllTodos(event: Event): void {
    const target = event.target as HTMLInputElement;
    this.todoService.toggleAll(target.checked);
  }

  setEditingId(editingId: string | null): void {
    this.editingId = editingId;
  }
}
